package hu.ngms.jpf.maven;


public class OverflowingValue {
	
	private int value;
	
	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int add(int value) {
		this.setValue(this.getValue() + value);
		return value;
	}
	
	public static void main(String[] args) {
		OverflowingValue value = new OverflowingValue();
		value.setValue(42);
		value.add(Integer.MAX_VALUE);
	}

}
