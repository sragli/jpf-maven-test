package hu.ngms.jpf.maven;

import gov.nasa.jpf.annotation.SandBox;

import java.util.ArrayList;
import java.util.List;

@SandBox
public class Sandbox {
	
	private List<String> properties = new ArrayList<String>();

	public void setProperty(List<String> properties, String property) {
		properties.add(property);
	}
	
	public void addProperty(String property) {
		properties.add(property);
	}
	
}
