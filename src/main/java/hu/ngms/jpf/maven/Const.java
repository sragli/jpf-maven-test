package hu.ngms.jpf.maven;

public class Const {
	
	@gov.nasa.jpf.annotation.Const
	private int a;
	private int b;
	
	public void changeConstant() {
		b = 10;
		a = 20;
	}

	public static void main(String[] args) {
	    new Const().changeConstant();
	}

}
