package hu.ngms.jpf.maven;

import java.util.Map;

public class NonNullExample {
	
	@javax.annotation.Nonnull
	private Map<String, String> map;

	public NonNullExample() {
	}
	
	public static void main(String[] args) {
		new NonNullExample();
	}

}
