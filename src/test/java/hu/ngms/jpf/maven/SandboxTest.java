package hu.ngms.jpf.maven;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class SandboxTest {

	private Sandbox sandbox;

	@Before
	public void setUp() throws Exception {
		sandbox = new Sandbox();
	}
	
	@Test
	public void testSandboxingError() {
		sandbox.setProperty(new ArrayList<String>(), "prop2");
	}

	@Test
	public void testSandboxingOk() {
		sandbox.addProperty("prop1");
	}

}
