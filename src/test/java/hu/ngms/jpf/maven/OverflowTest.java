package hu.ngms.jpf.maven;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class OverflowTest {
	
	private OverflowingValue value;

	@Before
	public void setUp() throws Exception {
		value = new OverflowingValue();
		value.setValue(42);
	}

	@Test
	public void shouldNotOverflow() {
		int actual = value.getValue();
		assertTrue(actual == actual + value.add(42) - 42);
	}

	@Test
	public void shouldOverflow() {
		int actual = value.getValue();
		assertTrue(actual == actual + value.add(Integer.MAX_VALUE) - Integer.MAX_VALUE);
	}

}
