package hu.ngms.jpf.maven;

import org.junit.Test;

public class ThreadingTest {

	@Test
	public void testDeadlock() {
		new Threading().deadlock();
	}

	@Test
	public void testRaceCondition() {
		new Threading().race();
	}

}
